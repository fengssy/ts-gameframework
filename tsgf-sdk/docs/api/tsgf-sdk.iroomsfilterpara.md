<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [IRoomsFilterPara](./tsgf-sdk.iroomsfilterpara.md)

## IRoomsFilterPara interface

房间列表的筛选参数

<b>Signature:</b>

```typescript
export interface IRoomsFilterPara 
```

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [maxPlayers?](./tsgf-sdk.iroomsfilterpara.maxplayers.md) | number | <i>(Optional)</i> |
|  [roomNameFullMatch?](./tsgf-sdk.iroomsfilterpara.roomnamefullmatch.md) | string | <i>(Optional)</i> |
|  [roomNameLike?](./tsgf-sdk.iroomsfilterpara.roomnamelike.md) | string | <i>(Optional)</i> |
|  [roomType?](./tsgf-sdk.iroomsfilterpara.roomtype.md) | string | <i>(Optional)</i> |

